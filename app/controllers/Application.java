package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static play.data.Form.form;

public class Application extends Controller {

    final static Form<GuestForm> guestForm = form(GuestForm.class);
    final static Form<RestoForm> restoForm = form(RestoForm.class);

    public Result index() {
        return ok(index.render(""));
    }
    public Result index(String message) {
        return ok(index.render(message));
    }

    public Result addGuest(){
        Guest g = new Guest();
        Form<GuestForm> filledForm = form(GuestForm.class).bindFromRequest();
        GuestForm gf = null;
        try{
            gf = filledForm.get();
        }
        catch (IllegalStateException e){
            return redirect(routes.Application.index("Ошибка при добавлении пользователя: неправильно заполнена форма"));
        }
        g.setName(gf.name);
        g.setPhone(gf.phone);
        if(!validatePhone(gf.phone)){
            return badRequest(index.render("Неправильно введен телефон"));
        }


        try{
            DBWorker.getInstance().getGuestDAO().addGuest(g);
        }
        catch(Exception e){
            return badRequest(index.render("Что-то не так"));
        }

        return ok(index.render("Гость добавлен"));
    }

    public Result showGuestCard(int id) {
        Guest guest = new Guest();
        try {
            guest = DBWorker.getInstance().getGuestDAO().getGuestById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ok(guestCard.render(guest, form(GuestForm.class)));
    }
    public Result showRestoCard(int id) {
        Resto resto = new Resto();
        try {
            resto = DBWorker.getInstance().getRestoDAO().getRestoById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ok(restoCard.render(resto, form(RestoForm.class)));
    }


    public Result updateGuest(int id)
    {
        try{
            Form<GuestForm> filledForm = form(GuestForm.class).bindFromRequest();
            GuestForm gf = filledForm.get();
            Guest guest = DBWorker.getInstance().getGuestDAO().getGuestById(id);
            guest.setName(gf.name);
            guest.setPhone(gf.phone);
            if(!validatePhone(gf.phone)){
                return badRequest(index.render("Неправильно введен телефон"));
            }
            DBWorker.getInstance().getGuestDAO().updateGuest(guest);
        }
        catch (Exception e)
        {
            return badRequest(index.render("Ошибка, проверьте уникальность номера"));
        }

        return redirect(routes.Application.index("Ok"));
    }

    public Result updateResto(int id){
        try{
            Form<RestoForm> filledForm = form(RestoForm.class).bindFromRequest();
            RestoForm rf = filledForm.get();
            Resto resto = DBWorker.getInstance().getRestoDAO().getRestoById(id);
            resto.setName(rf.name);
            DBWorker.getInstance().getRestoDAO().updateResto(resto);
        }
        catch (Exception e)
        {
            return badRequest(index.render("Ошибка updateResto"));
        }

        return redirect(routes.Application.index(""));
    }

    public static List<Guest> getAllGuests(){
        List<Guest> res = null;
        try {
            res = DBWorker.getInstance().getGuestDAO().getAllGuests();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<Resto> getAllResto(){

        List<Resto> resto = new ArrayList<>();
        try {
            resto = DBWorker.getInstance().getRestoDAO().getAllResto();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resto;
    }
    public Result addResto(){
        Resto r = new Resto();
        Form<RestoForm> filledForm = form(RestoForm.class).bindFromRequest();
        RestoForm rf = filledForm.get();
        r.setName(rf.name);
        try{
            DBWorker.getInstance().getRestoDAO().addResto(r);
        }
        catch(Exception e){
            return badRequest(index.render("Что-то не так"));
        }

        return ok(index.render("Ресторан добавлен"));
    }

    public static List<Resto> getRestoByUserId(int userId){
        List<Resto> result =  DBWorker.getInstance().getRestoGuestDAO().getRestoByGuestId(userId);
        return result;
    }
    public static List<Guest> getGuestByRestoId(int restoId){
        List<Guest> result =  DBWorker.getInstance().getRestoGuestDAO().getGuestsByRestoId(restoId);
        return result;
    }


    public static List<Resto> getUnreachedRestoByUserId(int userId){
        List<Resto> result = null;
        try {
            result = DBWorker.getInstance().getRestoDAO().getAllResto();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Resto> resto = DBWorker.getInstance().getRestoGuestDAO().getRestoByGuestId(userId);
        result.removeAll(resto);
        return result;
    }

    public static List<Guest> getUnreachedGuestsByRestoId(int restoId){
        List<Guest> result = null;
        try {
            result = DBWorker.getInstance().getGuestDAO().getAllGuests();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Guest> guests = DBWorker.getInstance().getRestoGuestDAO().getGuestsByRestoId(restoId);
        result.removeAll(guests);

        return result;
    }

    public Result addRestoGuest(int restoId, int guestId, String rs){
        Resto resto = null;
        Guest guest = null;
        try {
            resto = DBWorker.getRestoDAO().getRestoById(restoId);
            guest = DBWorker.getGuestDAO().getGuestById(guestId);
            DBWorker.getInstance().getRestoGuestDAO().addRestoGuest(new RestoGuest(resto,guest));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(rs.equals("Resto"))
        {
            return redirect(routes.Application.showRestoCard(restoId));
        }
        if(rs.equals("Guest"))
        {
            return redirect(routes.Application.showGuestCard(guestId));
        }
        return ok();
    }

    public Result remRestoGuest(int restoId, int guestId,  String rs){
        Resto resto = null;
        Guest guest = null;
        try {
            resto = DBWorker.getRestoDAO().getRestoById(restoId);
            guest = DBWorker.getGuestDAO().getGuestById(guestId);
            DBWorker.getInstance().getRestoGuestDAO().deleteRestoGuest(new RestoGuest(resto,guest));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(rs.equals("Resto"))
        {
            return redirect(routes.Application.showRestoCard(restoId));
        }
        if(rs.equals("Guest"))
        {
            return redirect(routes.Application.showGuestCard(guestId));
        }
        return ok();
    }

    private boolean validatePhone(String phone){
        Pattern p = Pattern.compile("^[+]7[ ][(][0-9]{3}[)][ ][0-9]{3}[ ][0-9]{2}[ ][0-9]{2}$");
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
