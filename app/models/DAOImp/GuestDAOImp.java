package models.DAOImp;

import models.DAO.GuestDAO;
import models.Guest;
import models.HibernateUtil;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public class GuestDAOImp implements GuestDAO {
    @Override
    public void addGuest(Guest guest) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(guest);
            session.getTransaction().commit();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при добавлении гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Guest getGuestById(int id) throws SQLException {
        Session session = null;
        Guest guest = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            guest = (Guest)session.get(Guest.class, id);
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка поиске гостя по ID", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
        return guest;
    }

    @Override
    public List<Guest> getAllGuests() {
        Session session = null;
        List<Guest>  guests= new ArrayList<Guest>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            guests = session.createCriteria(Guest.class).list();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка запросе всез гостей", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return guests;
    }

    @Override
    public void deleteGuest(Guest guest) throws SQLException {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(guest);
            session.getTransaction().commit();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при удалении гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if (session !=null && session.isOpen()){
                session.close();
            }
        }
    }

    @Override
    public void updateGuest(Guest guest) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(guest);
            session.getTransaction().commit();
        }
        catch (Exception e) {
            //JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при обновлении гостя", JOptionPane.OK_OPTION);
            if(session != null && session.isOpen()){
                session.close();
            }
            throw new IllegalStateException("Ошибка при обновлении гостя");
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }
}
