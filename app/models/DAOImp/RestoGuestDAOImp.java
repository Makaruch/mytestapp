package models.DAOImp;

import models.DAO.RestoGuestDAO;
import models.Guest;
import models.HibernateUtil;
import models.Resto;
import models.RestoGuest;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public class RestoGuestDAOImp implements RestoGuestDAO {
    @Override
    public void addRestoGuest(RestoGuest restoguest) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(restoguest);
            session.getTransaction().commit();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при добавлении гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public RestoGuest getRestoGuestById(int id) throws SQLException {
        Session session = null;
        RestoGuest restoguest = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            restoguest = (RestoGuest)session.load(RestoGuest.class, id);
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка поиске гостя по ID", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
        return restoguest;
    }

    @Override
    public List<RestoGuest> getAllRestoGuests() throws SQLException {
        Session session = null;
        List<RestoGuest>  restoguests= new ArrayList<RestoGuest>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            restoguests = session.createCriteria(RestoGuest.class).list();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка запросе всез гостей", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return restoguests;
    }

    @Override
    public void deleteRestoGuest(RestoGuest restoguest) {
        Session session = null;
        List<RestoGuest>  restoguests= new ArrayList<RestoGuest>();
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            restoguests = session.createCriteria(RestoGuest.class).list();
            for(RestoGuest r:restoguests){
                if (restoguest.getGuest().equals(r.getGuest())&&restoguest.getResto().equals(r.getResto())){
                    session.beginTransaction();
                    session.delete(r);
                    session.getTransaction().commit();
                }
            }

        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при удалении гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if (session !=null && session.isOpen()){
                session.close();
            }
        }
    }

    @Override
    public void updateRestoGuest(RestoGuest restoguest) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(restoguest);
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при обновлении RestoGuest", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }

    public List<Resto> getRestoByGuestId(int guestId)
    {
        List<Resto> result = null;
        Session session = null;
        try
        {
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery(String.format("select distinct resto from Resto as resto," +
                    " RestoGuest restoGuest join restoGuest.resto as rgResto join restoGuest.guest as rgGuest" +
                    " where rgGuest.id = %d and resto.id = rgResto.id", guestId)).list();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка выбора ресторанов по id гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
        return result;

    }
    public List<Guest> getGuestsByRestoId(int restoId){
        List<Guest> result = null;
        Session session = null;
        try
        {
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery(String.format("select distinct guest from Guest as guest," +
                    " RestoGuest restoGuest join restoGuest.resto as rgResto join restoGuest.guest as rgGuest" +
                    " where rgGuest.id = guest.id and %d = rgResto.id", restoId)).list();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка выбора ресторанов по id гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }

        return result;
    }
}
