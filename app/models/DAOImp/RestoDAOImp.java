package models.DAOImp;

import models.DAO.RestoDAO;
import models.HibernateUtil;
import models.Resto;
import org.hibernate.Session;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public class RestoDAOImp implements RestoDAO {
    @Override
    public void addResto(Resto resto) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(resto);
            session.getTransaction().commit();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при добавлении гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Resto getRestoById(int id) throws SQLException {
        Session session = null;
        Resto resto = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            resto = (Resto)session.get(Resto.class, id);
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка поиске ресторана по ID", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
        return resto;
    }

    @Override
    public List<Resto> getAllResto(){
        Session session = null;
        List<Resto>  restos= new ArrayList<Resto>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            restos = session.createCriteria(Resto.class).list();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка запросе всех рестов", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return restos;
    }

    @Override
    public void deleteResto(Resto resto) throws SQLException {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(resto);
            session.getTransaction().commit();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при удалении ресторана", JOptionPane.OK_OPTION);
        }
        finally {
            if (session !=null && session.isOpen()){
                session.close();
            }
        }
    }

    @Override
    public void updateResto(Resto resto) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(resto);
            session.getTransaction().commit();
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null,e.getMessage(),"Ошибка при обновлении ресторана", JOptionPane.OK_OPTION);
        }
        finally {
            if(session != null && session.isOpen()){
                session.close();
            }
        }
    }
}

