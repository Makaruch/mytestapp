package models;

import javax.persistence.*;

/**
 * Created by Serge on 22.06.2015.
 */
@Entity
@Table(name = "RestoGuest")
public class RestoGuest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @JoinColumn(name = "resto")
    private Resto resto;
    @ManyToOne
    @JoinColumn(name="guest")
    private Guest guest;

    public RestoGuest(){
        this.resto = null;
        this.guest = null;
    }
    public RestoGuest(Resto resto, Guest guest) {
        this.resto = resto;
        this.guest = guest;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setGuest(Guest guest) {
        this.guest = guest;
    }

    public void setResto(Resto resto) {
        this.resto = resto;
    }

    public Guest getGuest() {
        return guest;
    }

    public Resto getResto() {
        return resto;
    }
}