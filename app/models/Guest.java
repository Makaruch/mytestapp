package models;

/**
 * Created by Serge on 22.06.2015.
 */
import javax.persistence.*;

@Entity
@Table(name = "Guest")
public class Guest {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name="name")
    private String name;

    @Column(name="phone", unique = true)
    private String phone;

    public Guest(){
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean equals(Object obj){
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (obj.getClass() == this.getClass()){
            Guest g = (Guest)obj;
            if((g.id == this.id)&&(g.name.equals(this.name)) &&(g.phone.equals(this.phone))){
                return true;
            }
        }
        return false;
    }
}
