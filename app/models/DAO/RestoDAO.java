package models.DAO;

/**
 * Created by Serge on 10.06.2015.
 */
import models.Resto;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public interface RestoDAO {
    public void addResto(Resto resto) throws SQLException;

    public void updateResto(Resto resto) throws SQLException;

    public Resto getRestoById(int id) throws SQLException;

    public List getAllResto() throws SQLException;

    public void deleteResto(Resto resto) throws SQLException;
}
