package models.DAO;

import models.Guest;
import models.Resto;
import models.RestoGuest;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public interface RestoGuestDAO {
    public void addRestoGuest(RestoGuest restoguest) throws SQLException;

    public void updateRestoGuest(RestoGuest restoguest) throws SQLException;

    public RestoGuest getRestoGuestById(int id) throws SQLException;

    public List getAllRestoGuests() throws SQLException;

    public void deleteRestoGuest(RestoGuest restoguest) throws SQLException;

    public List<Resto> getRestoByGuestId(int guestId);

    public List<Guest> getGuestsByRestoId(int restoId);

}
