package models.DAO;

import models.Guest;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serge on 10.06.2015.
 */
public interface GuestDAO {
    public void addGuest(Guest guest) throws SQLException;

    public void updateGuest(Guest guest) throws SQLException;

    public Guest getGuestById(int id) throws SQLException;

    public List getAllGuests() throws SQLException;

    public void deleteGuest(Guest guest) throws SQLException;
}
