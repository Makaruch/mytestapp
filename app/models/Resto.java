package models;

import javax.persistence.*;

/**
 * Created by Serge on 05.06.2015.
 */

@Entity
@Table(name = "Resto")

public class Resto {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (obj.getClass() == this.getClass()){
            Resto g = (Resto)obj;
            if((g.id == this.id)&&(g.name.equals(this.name))){
                return true;
            }
        }
        return false;
    }
}

