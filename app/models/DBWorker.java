package models;

import models.DAO.*;
import models.DAOImp.GuestDAOImp;
import models.DAOImp.RestoDAOImp;
import models.DAOImp.RestoGuestDAOImp;

import java.util.List;

import org.hibernate.Session;

import javax.swing.*;

/**
 * Created by Serge on 22.06.2015.
 */
public class DBWorker {

    private static DBWorker instance;
    private static GuestDAO guestDAO;
    private static RestoDAO restoDAO;
    private static RestoGuestDAO restoGuestDAO;

    public static synchronized DBWorker getInstance(){
        if (instance == null){
            instance = new DBWorker();
        }
        return instance;
    }
    public static synchronized GuestDAO  getGuestDAO()
    {
        if(guestDAO == null){
            guestDAO = new GuestDAOImp();
        }
        return guestDAO;
    }
    public static synchronized RestoDAO getRestoDAO(){
        if(restoDAO == null){
            restoDAO = new RestoDAOImp();
        }
        return restoDAO;
    }
    public static synchronized RestoGuestDAO getRestoGuestDAO(){
        if(restoGuestDAO == null){
            restoGuestDAO = new RestoGuestDAOImp();
        }
        return restoGuestDAO;
    }

    public List<String> getRestoNamesByGuestId(int guestId){
        List<String> result = null;
        Session session = null;
        try
        {
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery(String.format("select distinct resto.Name from Resto as resto," +
                    " RestoGuest restoGuest join restoGuest.resto as rgResto join restoGuest.guest as rgGuest" +
                    " where rgGuest.id = %d and resto.id = rgResto.id", guestId)).list();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка выбора ресторанов по id гостя", JOptionPane.OK_OPTION);
        }
        finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
        return result;
    }

    public List<String> getGuestNamesByRestoId(int restoId)
    {
        List<String> result = null;
        Session session = null;

        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery(String.format("select distinct guest.name from Guest as guest," +
                    " RestoGuest restoGuest join restoGuest.resto as rgResto join restoGuest.guest as rgGuest" +
                    " where rgResto.id = %d and guest.id = rgGuest.id", restoId)).list();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка выбора гостей по id ресторана", JOptionPane.OK_OPTION);
        }
        finally {
            if(session !=null && session.isOpen())
            {
                session.close();
            }
        }
        return result;
    }


}
