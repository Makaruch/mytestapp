package models;

import play.data.validation.Constraints;

/**
 * Created by Serge on 06.07.2015.
 */
public class GuestForm {
    @Constraints.Required
    public String name;
    @Constraints.Required
    public String phone;
}
